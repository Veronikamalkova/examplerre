FROM rocker/tidyverse:4.2.1

MAINTAINER Lukas Novak <lukasjirinovak@gmail.com>

# before step (in gitlab)
# - update, and set for maximum cpu use
# - make a renv folder and install renv
RUN apt-get update
#COPY . /

# this works as it should be, except files are no visible in Rstudio server
#....................
COPY . /home/rstudio
#COPY . /Home/
#....................

#WORKDIR /home/rstudio

RUN apt-get install -y git

#.....Download repo inside image if needed
# RUN apt-get install -y git
# RUN git clone https://oauth2:glpat-9AxiZCutuwyt5sxf3VRX@gitlab.com/lukas.novak/RRE.git
#..........................................
#RUN echo "setwd(\"/home/RRE/\")" > ~/../home/rstudio/.Rprofile #set up working diretory
#COPY .Rprofile /home/rstudio/
#COPY .Rprofile /home/


# Change file ownership
#RUN chown -R rstudio:rstudio /home/rstudio/

ARG RSTUDIO_PATH=/home/rstudio

# WORKDIR ${RSTUDIO_PATH}}
# For mounting
#RUN chmod a+rwx -R ${RSTUDIO_PATH}
#CMD ["/init"]


#COPY .gitconfig /~/

#RUN mkdir files
#COPY files/ /files/
#RUN ls -la /files/*

#COPY home ./
#COPY . home/   # adds "test" to `WORKDIR`/relativeDir/
#RUN ls -la /home/*
#COPY pokus_doc.Rmd /home/pokus_doc.Rmd
#RUN echo "options(Ncpus = $(nproc --all))" >> /usr/local/lib/R/etc/Rprofile.site
#RUN mkdir -p ~/.local/share/renv
#RUN R -e 'install.packages("renv")'
# user settings
# - install the systems libraries
# - copy script and lock file
#RUN apt-get install -y --no-install-recommends libcurl4-openssl-dev libssl-dev libxt6
# COPY run_job.R run_job.R
# I found that renv::restore does not use the super fast
#   rstudio package manager, and so by pre instaling rtweet and ggplot2
#   and all their dependencies we get way faster building speed
# RUN R -e 'renv::restore()'
# on running of the container this is called:
# CMD Rscript run_job.R
