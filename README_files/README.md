# **R projects template in Docker**

To fully reproduce the study results (on Windows), there is need to undertake the following steps:

### 1. Download all files from the project from [Gitlab](https://gitlab.com/lukas.novak/RRE/-/archive/master/RRE-master.zip)

### 2. Unzip/exctract downloaded zip file
<img src="/README_files/How-to-extract-a-ZIP-File-on-Windows-10.jpg" width="600" height="400"/>
<!-- (https://www.hellotech.com/guide/wp-content/uploads/2019/12/How-to-extract-a-ZIP-File-on-Windows-10.jpg) -->

### 3. Click `extract`
<img src="/README_files/extract.png" width="600" height="400"/>

### 4. Open folder, find file named as `project_start.ps1`, right click and select open `Run with PowerShell` ![](/powershell_run.jpg)

### 5. After Windows PowerShell opens it installs necessary project components. This process can take a bit. [Docker desktop](/docker_img.jpg) should appear - you can minimize this program ![](/README_files/open_powershell_w.jpg)

### 6. Finally, you should see in your web browser new tab in which R studio can be found  ![](/r_studio_server.jpg)

### 7. Click on R Markdown File `.Rmd`  located in project directory  ![](/r_studio_server_rmd.jpg)

### 8. Click on `knit` button  ![](/rmarkdown_knit.jpg)

## After [R studio](https://www.r-project.org/) finishes the knit operation, you should be able to download report with fully reproduced study results