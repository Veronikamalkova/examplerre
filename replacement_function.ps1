#Uncheck if there are problems with premissions
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#...............................................................................................
#                Replace values with "_new" suffix with desired values
#...............................................................................................
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
# New project - example
$docker_user_new = "exampleuser3"
$docker_project_new = "examplerre"
$docker_project_and_user_new = "exampleuser3/examplerre:latest"
$full_project_name_new = "Example project"
$image_version_new = "rocker/tidyverse:4.2.1"
$MAINTAINER_new = "Lukas Novak <lukasjirinovak@gmail.com>"
$git_user_name_new = "Veronikamalkova"
$git_user_email_new = "malkova25@seznam.cz"
$git_url_new = "https://gitlab.com/Veronikamalkova/examplerre.git"
$R_project_name_new = "examplerre"
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
#############################################################################################xxx
# Old project - DO NOT EDIT !!!
$docker_user_old = "user"
$docker_project_old = "project_name"
$docker_project_and_user_old = "user/project:latest"
$full_project_name_old = "R projects template in Docker"
$image_version_old = "rocker/image:tag"
$MAINTAINER_old = "Name Surname <email@gmail.com>"
$git_user_name_old = "git_username"
$git_user_email_old = "email@gmail.com"
$git_url_old= "https://gitlab.com/git.user.name/project.git"
$R_project_name_old = "dasdf"
#...............................................................................................
# Script from:
# https://stackoverflow.com/questions/2837785/powershell-script-to-find-and-replace-for-all-files-with-a-specific-extension

Write-Host "Replacing in PowerShell files"
$configFiles = Get-ChildItem "$PSScriptRoot\project_start.ps1" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $docker_project_and_user_old, $docker_project_and_user_new } |
            Set-Content $file.PSPath
}

Write-Host "Replacing project and user in gitlab-ci.yml"
$configFiles = Get-ChildItem "$PSScriptRoot\.gitlab-ci.yml" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace "/$docker_project_and_user_old", "/$docker_project_and_user_new" } |
            Set-Content $file.PSPath
}

Write-Host "Replacing Docker project in gitlab-ci.yml"
$configFiles = Get-ChildItem "$PSScriptRoot\.gitlab-ci.yml" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $docker_project_old, $docker_project_new } |
            Set-Content $file.PSPath
}

Write-Host "Replacing name of the project in markdown file"
$configFiles = Get-ChildItem "$PSScriptRoot\.README.md" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $full_project_name_old, $full_project_name_new } |
            Set-Content $file.PSPath
}

Write-Host "Replacing in DockerFile"
$configFiles = Get-ChildItem "$PSScriptRoot\Dockerfile" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $image_version_old, $image_version_new } |
            Set-Content $file.PSPath
}


Write-Host Mainterner was changed from $MAINTAINER_old to $MAINTAINER_new
$configFiles = Get-ChildItem "$PSScriptRoot\Dockerfile" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $MAINTAINER_old, $MAINTAINER_new } |
            Set-Content $file.PSPath
}

Write-Host Git username was changed from $git_user_name_old to $git_user_name_new in .Rprofile
$configFiles = Get-ChildItem "$PSScriptRoot\.Rprofile" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $git_user_name_old, $git_user_name_new } |
            Set-Content $file.PSPath
}

Write-Host Git email was changed from $git_user_email_old to $git_user_email_new in .Rprofile
$configFiles = Get-ChildItem "$PSScriptRoot\.Rprofile" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $git_user_email_old, $git_user_email_new } |
            Set-Content $file.PSPath
}

Write-Host Git url was changed from $git_url_old to $git_url_new in .Rprofile
$configFiles = Get-ChildItem "$PSScriptRoot\.Rprofile" -rec
foreach ($file in $configFiles)
{
    (Get-Content $file.PSPath) |
            Foreach-Object { $_ -replace $git_url_old, $git_url_new } |
            Set-Content $file.PSPath
}

Write-Host name of the project was changed from $R_project_name_old to $R_project_name_new
$files = Get-ChildItem "$PSScriptRoot\dasdf.Rproj" -rec
foreach ($file in $files) {
    $file | Rename-Item -NewName { $_ -replace $R_project_name_old, $R_project_name_new }
}

Write-Host "Replacement of old names finished succesfully, you can close PowerShell window now"

PowerShell -NoExit
